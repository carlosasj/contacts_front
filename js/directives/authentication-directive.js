angular.module("ContactsApp")
    .directive('showLogin', ['$route', 'Notification', '$rootScope', function($route, Notification, $rootScope) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var login = $element.find('#login-holder');
                // var loginError = $element.find('#login-error');
                var main = $element.find('#content');
                var username = $element.find('#username');
                var password = $element.find('#password');

                login.hide();

                $scope.$on('event:auth-loginRequired', function() {
                    main.hide();
                    login.show();
                    $rootScope.localUsername = null;
                });
                $scope.$on('event:auth-loginFailed', function() {
                    Notification.error("Login failed. Please check your username and password");
                    $rootScope.localUsername = null;
                });
                $scope.$on('event:auth-loginConfirmed', function() {
                    main.show();
                    login.hide();
                });
                $scope.$on('event:auth-socialLoginConfirmed', function() {
                    main.show();
                    login.hide();
                    $route.reload();
                });
                $scope.$on('event:auth-hideLogin', function() {
                    main.show();
                    login.hide();
                });
            }
        }
    }]);

function getLocalToken() {
    return localStorage["authToken"] || 'AnotherEmptyInvalidTokenOf32char';
}

function setLocalToken(value) {
    localStorage["authToken"] = value;
}

function getHttpConfig(extra) {
    var header = { headers: { 'X-Auth-Token':getLocalToken() } };

    if (!(typeof extra === "undefined")) {
        // Merge header with extra, recursive
        // a = { headers: { 'X-Auth-Token': 'local token' } };

        // b = { headers: { 'outrakey': 'asdfg hjklç' },
        //       fatherkey: { 'maiskey': 'qwertyuiop' }
        //     }

        // result = $.extend(true, a, b)
        // result => { headers: { 'X-Auth-Token': 'local token',
        //                        'outrakey': 'asdfg hjklç' },
        //             fatherkey: { 'maiskey': 'qwertyuiop' }
        //           }
        header = $.extend(true, header, extra)
    }

    return header;
}

function getAuthenticateHttpConfig() {
    return {
        ignoreAuthModule: true
    };
}
