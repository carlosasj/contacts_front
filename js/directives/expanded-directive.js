angular.module('ContactsApp').
	directive('ctCard', function() {
		return {
			restrict: "E",
			templateUrl: "templates/pages/contacts/one_card.html",
		}
	});
