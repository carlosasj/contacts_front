angular.module('ContactsApp')
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.bind('change',function () {
                    $parse(attrs.fileInput).assign(scope,  el[0].files);
                    scope.$apply();
                })
            }
        }
    }]);
