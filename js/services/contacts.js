angular.module('ContactsApp')
    .factory('Contact', [ "$http", "Upload", function ContactFactory($http, Upload) {
        return {
            all: function () {
                return $http.get(urlpath("contact/getContactList/"), getHttpConfig());
            },
            get: function (id) {
                return $http.get(urlpath("contact/getContact/"+id), getHttpConfig());
            },
            set: function (data, files) {
                data = clean(data);
                if (typeof files === 'undefined') {
                    return $http.post(urlpath("contact/addContact/"), data, getHttpConfig());
                } else {
                    return Upload.upload({
                        method: 'POST',
                        url: urlpath('contact/addContact/'),
                        data: $.extend({file: files}, data),
                        headers: { 'X-Auth-Token':getLocalToken() }
                    });
                }
            },
            delete: function (id) {
                return $http.post(urlpath("contact/delContact/"), {"id": id}, getHttpConfig());
            }
        }
    }]);
