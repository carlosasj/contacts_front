angular.module('ContactsApp')
    .factory('Account', [ "$http", function AccountFactory($http) {
        return {
            set: function (data) {
                return $http.post(urlpath("authenticate/createAccount/"), data);
            },
            getUsernameByAuthToken: function (token) {
                return $http.post(urlpath("authenticate/getUsernameByAuthToken/"), {token: token});
            },
            validate: function (token) {
                return $http.post(urlpath("authenticate/validateMail/"), {token: token});
            }
        }
    }]);
