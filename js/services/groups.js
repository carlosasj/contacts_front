angular.module('ContactsApp')
    .factory('Group', [ "$http", function GroupFactory($http) {
        return {
            all: function () {
                return $http.get(urlpath("contactsGroup/getGroupsList/"), getHttpConfig());
            },
            get: function (id) {
                return $http.get(urlpath("contactsGroup/getGroup/"+id), getHttpConfig());
            },
            getContacts: function (id) {
                return $http.get(urlpath("contactsGroup/getGroupContacts/"+id), getHttpConfig());
            },
            set: function (data) {
                return $http.post(urlpath("contactsGroup/addGroup/"), data, getHttpConfig());
            },
            delete: function (id) {
                return $http.post(urlpath("contactsGroup/delGroup/"), {"id": id}, getHttpConfig());
            }
        }
    }]);
