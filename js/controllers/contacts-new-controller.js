angular.module('ContactsApp')
    .controller('contactsNewCtrl', ['$scope', '$routeParams', 'Contact', 'Group', '$location', 'Notification',
        function ($scope, $routeParams, Contact, Group, $location, Notification) {
        $scope.template_action = 'Add';
        $.material.options.autofill = true;
        $.material.init();
        Group.all()
            .success(function (data) {
                $scope.all_groups = data.groups_list;
            }).catch(function (data) {
                Notification.error("Unable to locate your Groups. Please, try to reload the page.");
            });

        $scope.submitForm = function () {
            $scope.formerror = {};
            Contact.set($scope.formdata, $scope.f_photo)
                .success(function (data) {
                    if (data.message == 'OK') {
                        Notification.success("Contact successfuly created.");
                        $location.path('/');
                    } else {
                        for (var i in data.errors){
                            var e = data.errors[i];
                            $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        }
                    }
                })
                .catch(function (data) {
                    for (var i in data.errors){
                        var e = data.errors[i];
                        $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                    }
                    Notification.error("Unable to send the form. Please, try again.");
                });
        }
    }]);
