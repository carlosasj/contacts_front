angular.module("ContactsApp")
    .controller('socialAuthCtrl',["$location", "$rootScope", "Account", "Notification",
        function ($location, $rootScope, Account, Notification) {
        var token = QueryString.token;
        setLocalToken(token);
        Account.getUsernameByAuthToken(token)
            .success(function (data) {
                if (data.status == "OK")
                    setLocalUsername($rootScope, data.username);
                else
                    Notification.error("Some error ocurred in our server. Please try to login again.");
                $location.url(localStorage["pathnameBeforeAuthentication"]);
            }).catch(function (data) {
                Notification.error("Your authentication failed :(");
                $location.url(localStorage["pathnameBeforeAuthentication"]);
            });
    }]);
