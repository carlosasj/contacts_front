angular.module("ContactsApp")
	.controller("contactsDetailsCtrl", ["$scope", "$routeParams", "Contact", "Notification", "$location",
		function ($scope, $routeParams, Contact, Notification, $location) {

		Contact.get($routeParams.id)
			.success(function (data) {
				if (data.instance) {
					$scope.contact = data.instance;
					if (data.instance.birthday)
						$scope.contact.birthday = data.instance.birthday.substr(0, 10).split("-").reverse().join("/");
						defaultcrop();
				} else {
					Notification.error("Unable to locate this contact. Sorry.");
					$location.path('/');
				}
			}).error(function (data) {
				Notification.error("Oops... Something went wrong.");
				$location.path('/');
			});
			defaultcrop();

	}]);
