angular.module('ContactsApp')
    .controller('groupsNewCtrl', ['$scope', 'Group', '$location', 'Notification',
        function ($scope, Group, $location, Notification) {
        $scope.template_action = 'Add';
        $.material.input();
        $scope.submitForm = function () {
            $scope.formerror = {};
            Group.set($scope.formdata)
                .success(function (data) {
                    $scope.formerror = {};
                    if (data.message == 'OK') {
                        Notification.success("Contact successfuly created.");
                        $location.path('/groups/');
                    } else {
                        for (var i in data.errors){
                            var e = data.errors[i];
                            $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        }
                    }
                })
                .catch(function (data) {
                    for (var i in data.errors){
                        var e = data.errors[i];
                        $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                    }
                    Notification.error("Unable to send the form. Please, try again.");
                });
        }
    }]);
