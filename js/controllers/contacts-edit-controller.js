angular.module("ContactsApp")
    .controller("contactsEditCtrl", ["$scope", "$routeParams", "Contact", "Group", "$location", "Notification", "$q",
        function ($scope, $routeParams, Contact, Group, $location, Notification, $q) {
        $scope.template_action = "Edit";
        $.material.options.autofill = true;
        $.material.init();

        $q.all([Group.all(), Contact.get($routeParams.id)]).then(
            function (data) {

                if (data[0].data.message == data[1].data.message && data[0].data.message == "OK"){

                    $scope.all_groups = data[0].data.groups_list;

                    var cont = data[1].data.instance;

                    var groups_list = [];
                    for (var i in cont.groups){
                        var g = cont.groups[i];
                        groups_list.push(g.id);
                    }
                    $scope.filename = (cont.photo_path)? cont.photo_path.split('/')[5] : "";
                    $scope.formdata = {
                        f_id: cont.id,
                        f_name: cont.name,
                        f_phone1: cont.phone1,
                        f_phone2: cont.phone2,
                        f_email: cont.email,
                        f_address: cont.address,
                        f_birthday: (cont.birthday)? new Date(cont.birthday.substr(0, 10)) : null,
                        f_groups: groups_list
                    };
                } else {
                    Notification.error("Error while loading this contact. Sorry.");
                    $location.path('/');
                }
            }, function (data) {
                Notification.error("Error while loading this contact. Sorry.");
                $location.path('/');
            }
        );

        $scope.submitForm = function () {
            $scope.formerror = {};
            Contact.set($scope.formdata, $scope.f_photo)
                .success(function (data) {
                    if (data.message == 'OK') {
                        Notification.success("Contact successfuly edited.");
                        $location.path('/');
                    } else {
                        for (var i in data.errors){
                            var e = data.errors[i];
                            $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        }
                    }
                })
                .catch(function (data) {
                    for (var i in data.errors){
                        var e = data.errors[i];
                        $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                    }
                    Notification.error("Unable to send the form. Please, try again.");
                });
        };

        $scope.delete = function () {
            Contact.delete($scope.formdata.f_id)
                .success(function (data) {
                    if (data.message == 'OK') {
                        Notification.success("Contact successfuly deleted.");
                        $location.path('/');
                    } else {
                        Notification.error("Unable to delete the contact.");
                    }
                })
                .catch(function (data) {
                    Notification.error("Unable to delete the contact.");
                    $scope.errors = data.data.error;
                });
        };
    }]);
