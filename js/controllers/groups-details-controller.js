angular.module("ContactsApp")
	.controller("groupsDetailsCtrl", ["$scope", "$routeParams", "Group", "Notification", "$location", "$q",
		function ($scope, $routeParams, Group, Notification, $location, $q) {
		$q.all([Group.get($routeParams.id), Group.getContacts($routeParams.id)]).then(
			function (data) {
				if (data[0].data.message == data[1].data.message && data[0].data.message == "OK"){
					$scope.group = data[0].data.instance;
					$scope.contacts_list = data[1].data.contacts_list;
				} else {
					Notification.error("Unable to load this group. Sorry.");
					$location.path('/groups/');
				}
			}, function (data) {
				Notification.error("Unable to load this group. Sorry.");
				$location.path('/groups/');
			}
		);
	}]);
