angular.module("ContactsApp")
    .controller("groupsEditCtrl", ["$scope", "$routeParams", "Group", "$location", "Notification",
        function ($scope, $routeParams, Group, $location, Notification) {
        $scope.template_action = "Edit";
        Group.get($routeParams.id)
            .success(function (data) {
                $scope.formdata = {
                    f_id: data.instance.id,
                    f_name: data.instance.name
                };
            }).catch(function (data) {
                Notification.error("Unable to locate this group. Sorry.");
                $location.path('/groups/');
            });
        $scope.all_groups = [];

        $scope.submitForm = function () {
            $scope.formerror = {};
            Group.set($scope.formdata, $scope.f_photo)
                .success(function (data) {
                    if (data.message == 'OK') {
                        Notification.success("Group successfuly edited.");
                        $location.path('/groups/');
                    } else {
                        for (var i in data.errors){
                            var e = data.errors[i];
                            $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        }
                    }
                })
                .catch(function (data) {
                    for (var i in data.errors){
                        var e = data.errors[i];
                        $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                    }
                    Notification.error("Unable to send the form. Please, try again.");
                });
        };

        $scope.delete = function () {
            Group.delete($scope.formdata.f_id)
                .success(function (data) {
                    if (data.message == 'OK') {
                        Notification.success("Contact successfuly deleted.");
                        $location.path('/groups/');
                    }
                })
                .catch(function (data) {
                    Notification.error("Unable to delete the contact.");
                    $scope.errors = data.data.error;
                });
        };
    }]);
