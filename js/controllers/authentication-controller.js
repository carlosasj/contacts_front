var login = angular.module('login', []);

login.controller('loginController',
    ["$rootScope", "$scope", "$http", "authService", "$cookieStore", "$window",
    function ($rootScope, $scope, $http, authService, $cookieStore, $window) {

        $scope.logIn = function() {
            var username, password;
            try {
                username = $scope.authData.username;
                password = $scope.authData.password;
            } catch(TypeError) {
                username = password = false;
                $rootScope.$broadcast('event:auth-loginFailed');
            }
            if (username && password){
                $http.post(urlpath('api/login'), { username: username, password: password }, getAuthenticateHttpConfig())
                    .success(function(data) {
                        $cookieStore.put("loggedIn", "true");
                        $cookieStore.put("currentUser", data.username);
                        $scope.$emit('updateHeader');
                        setLocalToken(data.access_token);
                        localStorage["authToken"] = data.access_token;
                        setLocalUsername($rootScope, data.username);
                        authService.loginConfirmed({}, function(config) {
                            config.headers["X-Auth-Token"] = getLocalToken();
                            return config;
                        });
                        $scope.authData.username = "";
                        $scope.authData.password = "";
                    })
                    .error(function(data) {
                        $rootScope.$broadcast('event:auth-loginFailed', data);
                        $scope.authData.password = "";
                    });

                $scope.loginForm.$setPristine();
            }
        };

        $scope.authenticate = function (provider) {
            localStorage["pathnameBeforeAuthentication"] = $window.location.pathname;
            $window.location.href = urlpath("oauth/authenticate/"+provider)
        }
    }]
);

login.controller('logoutController',
    ['$rootScope', '$scope', '$http', '$location', '$cookieStore', '$route',
    function ($rootScope, $scope, $http, $location, $cookieStore, $route) {

        $scope.logOut = function() {

            $http.post(urlpath('api/logout'), {}, getHttpConfig())
                .success(function() {
                    $cookieStore.put("loggedIn", null);
                    $cookieStore.put("currentUser", null);
                    $scope.$emit('updateHeader');
                    localStorage.clear();
                    $rootScope.localUsername = false;
                    $location.path("/");
                    $route.reload();
                })
                .error(function(data) {
                    // localStorage.clear();
                });
        }
    }]
);

