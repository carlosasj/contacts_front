angular.module("ContactsApp")
    .controller('activateCtrl',["$location", "$rootScope", "Account", "Notification",
        function ($location, $rootScope, Account, Notification) {
            var token = QueryString.token;
            Account.validate(token)
                .success(function (data) {
                    if (data.status == "OK")
                        if (data.success){
                            Notification.success({message: "Your account is activated. Please login.", delay: null});
                        } else {
                            Notification.error({message: "An error ocurred: " + data.error_msg + '<br><small>(click to close)</small>', delay: null});
                        }
                    else
                        Notification.error({message: "We had a server error. Please try again later.", delay: null});
                    $location.url('/');
                }).catch(function (data) {
                    Notification.error({message: "Communication error. Please try again.", delay: null});
                    $location.url('/');
                });
        }]);
