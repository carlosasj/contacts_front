angular.module('ContactsApp')
    .controller('newAccountCtrl', ['$scope', '$rootScope', 'Account', 'Notification',
        function ($scope, $rootScope, Account, Notification) {
        $rootScope.$broadcast('event:auth-hideLogin');
        $scope.formerror = {};
        $.material.input();

        $scope.checkPassword = function () {
            var message;
            var pass = $scope.formdata['password'];
            var pass_repeat = $scope.formdata['password_again'];
            if (pass && pass_repeat) {
                message = (pass != pass_repeat)? "Passwords don't match" :"";
            } else {
                message = "";
            }

            $('#id_password_again')[0].setCustomValidity(message);
            $scope.formerror.password_again = message;
        };

        $scope.submitForm = function () {
            $scope.formerror = {};
            Account.set($scope.formdata)
                .success(function (data) {

                    if (data.status != 'SUCCESS') {
                        Notification.error("Error while creating user");
                        console.log(data);
                        for (var i in data.errors){
                            var e = data.errors[i];
                            $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        }
                    } else {
                        Notification.success({message: "User successfully created." +
                        "<br>We sent you an e-mail message to activate your account. Please verify your e-mail." +
                        "<br>If you don't receive a message in 10 minutes, check your Spam folder, then contact us.", delay: null});
                    }
                })
                .catch(function (data) {
                    for (var i in data.errors){
                        var e = data.errors[i];
                        $scope.formerror[e.field] = mask(smErrors[e.code], e.arguments);
                        Notification.error("Unable to send the form. Please, try again.");
                    }
                });
        };

        $scope.$on("$destroy", function(){
            $rootScope.$broadcast('event:auth-loginRequired');
        });
    }]);
