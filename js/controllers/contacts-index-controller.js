angular.module("ContactsApp")
    .controller("contactsCtrl", ["$scope", "$templateCache", "Contact", "Notification",
        function ($scope, $templateCache, Contact,Notification) {
        Contact.all()
            .success(function (data) {
                $scope.contacts = data.contacts_list;
            }).catch(function (data) {
                Notification.error("Unable to load your contacts. Please, try to reload the page.");
            });
    }]);
