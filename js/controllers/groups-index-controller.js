angular.module("ContactsApp")
    .controller("groupsCtrl", ["$scope", "$templateCache", "Group", "Notification",
        function ($scope, $templateCache, Group, Notification) {
        Group.all()
            .success(function (data) {
                $scope.groups = data.groups_list;
            }).catch(function (data) {
                Notification.error("Unable to load your groups. Please, try to reload the page.");
            });
    }]);
