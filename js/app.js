var domain = "http://ec2-52-202-129-153.compute-1.amazonaws.com/";
// var domain = "http://localhost:8080/";
function urlpath(path) {
	return domain+path;
}

function mask(string, items) {
	var str = string;
	for (var i in items){
		var arg = items[i];
		str = str.replace('[{'+i+'}]', arg);
	}
	return str;
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0; i < vars.length; i++) {
		var pair = vars[i].split("=");

		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = decodeURIComponent(pair[1]);

		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];

		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(decodeURIComponent(pair[1]));
		}
	}
	return query_string;
}();

function getLocalUsername() {
	return localStorage.localUsername || false;
}

function setLocalUsername($rootScope, value) {
	$rootScope.localUsername = value;
	localStorage.localUsername = value;
}

function clean(array) {
	for (var i in array) {
		if (array[i] === null || array[i] === undefined) {
			delete array[i];
		}
	}
	return array;
}

$(function () {
	$.material.init();
}());

angular.module("ContactsApp", [
	"http-auth-interceptor",
	"ngRoute",
	"ngCookies",
	"login",
	"ui-notification",
	"ngAnimate",
	"angular-loading-bar",
	"GoogleMapsNative",
	"ngFileUpload"
]).config(["NotificationProvider", "cfpLoadingBarProvider", function (NotificationProvider, cfpLoadingBarProvider) {
	NotificationProvider.setOptions({
		delay: 10000,
		startTop: 70,
		startRight: 10,
		verticalSpacing: 20,
		horizontalSpacing: 20,
		positionX: 'right',
		positionY: 'top'
	});
	cfpLoadingBarProvider.latencyThreshold = 600;
	cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
	cfpLoadingBarProvider.includeSpinner = false;
}]).run(['$rootScope', function($rootScope) {
	$rootScope.localUsername = getLocalUsername();
}]);
