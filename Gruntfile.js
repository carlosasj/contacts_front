module.exports = function(grunt) {

		'use strict';
	var gruntConfig = {

		pkg: grunt.file.readJSON('package.json'),

		// ========= Lint JS =========
		jshint: {
			all: ['js/*.js']
		},

		// ========= Concatenate JS =========
		concat: {
			personal: {
				src: [
					'bower_components/angular-http-auth/src/http-auth-interceptor.js',
					'bower_components/smartcrop/smartcrop.js',
					'js/**/*.js'
					],
				dest: 'partial/js/concat_custom.js'
			},
			all_nomin: {
				src: [
					'bower_components/jquery/dist/jquery.js',
					'bower_components/bootstrap/dist/js/bootstrap.js',
					'bower_components/bootstrap-material-design/dist/js/material.js',
					'bower_components/bootstrap-material-design/dist/js/ripples.js',
					'bower_components/angular/angular.js',
					'bower_components/angular-route/angular-route.js',
					'bower_components/angular-cookies/angular-cookies.js',
					'bower_components/angular-ui-notification/dist/angular-ui-notification.js',
					'bower_components/angular-animate/angular-animate.js',
					'bower_components/angular-loading-bar/build/loading-bar.js',
					'bower_components/angular-google-maps-native/dist/angular-google-maps-native.js',
					'bower_components/ng-file-upload/ng-file-upload.js',
					'bower_components/angular-http-auth/src/http-auth-interceptor.js',
					'bower_components/smartcrop/smartcrop.js',
					'js/**/*.js'
				],
				dest: 'prod/js/all.min.js'
			},
			all: {
				src: [
					'bower_components/jquery/dist/jquery.min.js',
					'bower_components/bootstrap/dist/js/bootstrap.min.js',
					'bower_components/bootstrap-material-design/dist/js/material.min.js',
					'bower_components/bootstrap-material-design/dist/js/ripples.min.js',
					'bower_components/angular/angular.min.js',
					'bower_components/angular-route/angular-route.min.js',
					'bower_components/angular-cookies/angular-cookies.min.js',
					'bower_components/angular-ui-notification/dist/angular-ui-notification.min.js',
					'bower_components/angular-animate/angular-animate.min.js',
					'bower_components/angular-loading-bar/build/loading-bar.min.js',
					'bower_components/angular-google-maps-native/dist/angular-google-maps-native.min.js',
					'bower_components/ng-file-upload/ng-file-upload.min.js',
					// 'bower_components/ng-img-crop/compile/minified/ng-img-crop.js',
					'partial/js/concat_custom.min.js'
				],
				dest: 'prod/js/all.min.js'
			}
		},

		// ========= Uglify JS =========
		uglify: {
			dist: {
				files: {
					'partial/js/concat_custom.min.js': ['partial/js/concat_custom.js']
				}
			}
		},

		// ========= Compile LESS =========
		less: {
			dist: {
				files: {
					'partial/css/angular-ui-notification.css': 'less/angular-ui-notification.less'
				}
			}
		},
		cssmin: {
			dist: {
				src: [
					'bower_components/bootstrap/dist/css/bootstrap.min.css',
					'bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.min.css',
					'bower_components/bootstrap-material-design/dist/css/ripples.min.css',
					'bower_components/angular/angular-csp.css',
					'bower_components/angular-loading-bar/build/loading-bar.min.css',
					// 'bower_components/ng-img-crop/compile/minified/ng-img-crop.css',
					'partial/css/*.css',
					'css/*.css'
					],
				dest: 'prod/css/all.min.css'
			}
		},

		// ========= Minify HTML =========
		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					collapseBooleanAttributes: true,
					decodeEntities: true,
					keepClosingSlash: true,
					removeComments: true,
					sortAttributes: true,
					sortClassName: true,
					useShortDoctype: true
				},
				files: [
					{
						expand: true,
						src: [ 'index.html' ],
						dest: 'prod/',
						filter: 'isFile'
					},
					{
						expand: true,
						src: [ 'templates/**/*.html' ],
						dest: 'prod/',
						filter: 'isFile'
					}
				]
			}
		},

		// ========= Copy IMAGES =========
		copy: {
			main: {
				files: [
					{
						expand:true,
						src: ['images/**'],
						dest: 'prod/images/'
					}
				]
			}
		},

		// ========= Clean semi-processed files =========
		clean: ['partial'],

		// ========= Files to WATCH =========
		watch: {
			dist: {
				files: ['js/**/*', 'css/**/*', 'images/**/*', 'less/**/*', 'templates/**/*', 'index.html'],
				tasks: ['nomin'],
				options: {
					spawn: false
				}
			}
		}
	};

	grunt.initConfig(gruntConfig);

	// load plugins
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.loadNpmTasks('grunt-changed');

	// Tasks
	grunt.registerTask('default', [
									'jshint',
									'concat:personal',
									'uglify',
									'concat:all',
									'less',
									'cssmin',
									'changed:htmlmin',
									'changed:copy',
									'clean'
								]);

	grunt.registerTask('nomin', [
		'jshint',
		'concat:all_nomin',
		'less',
		'cssmin',
		'changed:htmlmin',
		'changed:copy',
		'clean'
	]);

	grunt.registerTask('js', [
		'jshint',
		'concat:personal',
		'uglify',
		'concat:all',
		'clean'
	]);


};

